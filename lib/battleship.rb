class BattleshipGame
  attr_reader :player, :board

  def initialize(player, board)
    @player = player
    @board = board
  end

  #runs the game by calling play_turn until the game is over
  def play
  end

  #gets a guess from the player and makes an attack
  def play_turn
    attack(@player.get_play)
  end

  #marks the board at pos, destroying or replacing any ship that might have been there
  def attack(pos)
    row, col = pos
    @board.grid[row][col] = :x
  end


  #prints information on the current state of hte game, incl. board state and number of ships remaining.
  def display_status
  end

  def count
    @board.count
  end

  def game_over?
    @board.won?
  end

end
