#responsible for translating user input into positions of the form [x,y]
class HumanPlayer
  attr_reader :name

  def initialize(name)
    @name = name
  end

  def get_play
    puts "Please select row:"
    row = gets.chomp.to_i
    puts "Please select col:"
    col = gets.chomp.to_i
    [row,col]
  end
  
end
