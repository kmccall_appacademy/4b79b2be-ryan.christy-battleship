class Board
  attr_reader :grid

  def initialize(grid=Board.default_grid)
    @grid = grid
  end

  def self.default_grid
    Array.new(10){Array.new(10)}
  end

  def empty?(pos=[])
    if pos.empty?
      !@grid.flatten.any?{|el| el == :s}
    else
      row, col = pos
      @grid[row][col] == nil
    end
  end

  def full?
    @grid.flatten.all?{|el| el == :s}
  end

  def place_random_ship
    raise "error" if full?
    pos = generate_pos
    until self[pos] == :s
      self[pos] = :s if self[pos] == nil
    end
  end

  def generate_pos
    a = @grid.length
    b = @grid[0].length
    row = (0...a).to_a.shuffle.last
    col = (0...b).to_a.shuffle.last
    [row, col]
  end
  #prints the board, with marks on any spaces that have been fired on
  def display
  end

  #returns the number of valid targets (ships) remaining
  def count
    @grid.flatten.count(:s)
  end

  #randomly distribute ships across the board
  def populate_grid

  end

  def won?
    !@grid.flatten.any?{|el| el == :s}
  end

  def in_range?(pos)
  end

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end
end
